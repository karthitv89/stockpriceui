import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Stock } from '../entities/Stock';
import { environment } from 'src/environments/environment';



@Injectable({
    providedIn: 'root'
})


export class StockService {

    constructor(private httpClient: HttpClient) { }


    getUserRequests(): Observable<Stock[]> {
        const param = new HttpParams()
            .set('size', '100');

        return this.httpClient.get<Stock[]>(
            environment.apiUrl + "/stockPrice/latestList", { params: param }
        );
    }
}
