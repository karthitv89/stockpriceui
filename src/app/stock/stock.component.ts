import { Component, OnInit, OnDestroy } from '@angular/core';
import { interval, Subscription } from 'rxjs';
import { StockService } from '../services/stock.service';


@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.scss']
})
export class StockComponent  implements OnInit, OnDestroy {
  gridApi: any;
  mySubscription: Subscription;

 

ngOnDestroy() {
  this.mySubscription.unsubscribe();
}

  constructor(private stockService: StockService) {

  }
  ngOnInit(): void {
    this.addItems();
    const source = interval(10000);
    this.mySubscription = source.subscribe(val => this.addItems());
    
  }
  title = 'StockPriceUi';

  columnDefs = [
    { headerName: 'Symbol', field: 'symbol' },
    { headerName: 'Price', field: 'marketPrice' },
    { headerName: 'Trend', field: 'trend' }
  ];

  rowData = [];

  interval: any;

  addItems() {
    var newItems = [];
 
    this.stockService.getUserRequests().subscribe(data => {
      this.rowData = data;  
    }, error => {
      console.log(error)
    });
    
  }


  }
