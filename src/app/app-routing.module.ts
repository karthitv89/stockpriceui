import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StockComponent } from './stock/stock.component';


const routes: Routes = [
  { path: '', redirectTo: '/stockList', pathMatch: 'full'},
  { path:'stockList', component: StockComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
